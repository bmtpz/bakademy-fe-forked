const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", name: "home", component: () => import("../pages/Index.vue") },
      {
        path: "/login",
        name: "login",
        component: () => import("../pages/LoginPage.vue"),
      },
      {
        path: "/courses/:id",
        name: "course",
        component: () => import("../pages/CourseDetailsPage.vue"),
      },
      {
        path: "/users/:id/my-courses",
        name: "my-courses",
        component: () => import("../pages/MyCoursesPage.vue"),
      },
      {
        path: "/upload",
        name: "upload",
        component: () => import("../pages/upload.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
