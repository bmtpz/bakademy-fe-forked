import { reactive, readonly } from "vue";
import { api } from "../boot/axios";
const initial = {
  id: -1,
  username: "",
  eMail: "",
  token: "",
  isLogged: false,
  cart: [],
};
const state = reactive(initial);

const getters = {
  getUserId() {
    return state.id;
  },
  getUsername() {
    return state.username;
  },
  getEmail() {
    return state.eMail;
  },
  getToken() {
    return state.token;
  },
  getisLogged() {
    return state.isLogged;
  },
  getCart() {
    return state.cart;
  },
};

const setters = {
  addItemToCart(item) {
    if (!state.cart.some((e) => e.id === item.id)) {
      state.cart.push(item);
    }
  },
  setCart(cart) {
    state.cart = cart;
  },
  emptyCart() {
    state.cart = [];
  },
};

const methods = {
  updateState(user) {
    state.id = user.id;
    state.username = user.username;
    state.eMail = user.eMail;
    state.isLogged = true;
    state.token = user.token;
  },
  updateToken(token) {
    state.token = token;
  },
  async logout() {
    const body = { token: localStorage.getItem("token") };
    const response = await api.delete(
      "http://localhost:4000/api/v1/auth/logout",
      body
    );

    if (response.status === 204) {
      state.id = -1;
      state.username = "";
      state.email = "";
      state.isLogged = false;
      state.token = "";
      localStorage.removeItem("token");
    }
  },
  async refreshToken() {
    const body = {
      token: localStorage.getItem("token"),
    };
    return await api.post("http://localhost:4000/api/v1/token", body);
  },
  async getCoursesByCategory(id) {
    return await api.get(`/api/v1/courses/categories/${id}`);
  },
  async getCourses() {
    return await api.get(`/api/v1/courses/`);
  },
  async login(body) {
    return await api.post("http://localhost:4000/api/v1/auth/login", body);
  },
  async getCourseById(id) {
    return await api.get(`/api/v1/courses/${id}`);
  },
  async buyCourse(id) {
    return await api.put(`/api/v1/users/buy/${id}`);
  },
  async getMyCourses(id) {
    return await api.get(`/api/v1/courses/users/${id}`);
  },
  async createCourse(body) {
    return await api.post(`http://localhost:3000/api/v1/courses/`, body);
  },
};

export default {
  state: readonly(state),
  methods,
  getters,
  setters,
};
